// node native
import * as path from 'path';

export { path };

import * as smartpath from '@pushrocks/smartpath';
import * as smartfile from '@pushrocks/smartfile';
import * as smartpromise from '@pushrocks/smartpromise';

export { smartpath, smartfile, smartpromise };

// third party
import handlebars from 'handlebars';

export { handlebars };
